<?php


class Rusty_Chart_Model
{
    public const OPTION_NAME = 'rusty-inc-org-chart-tree';

    public function save_tree($tree) {
        update_option(self::OPTION_NAME, $tree, true);
    }
}
